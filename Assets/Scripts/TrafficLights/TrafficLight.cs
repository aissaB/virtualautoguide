﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : TrafficLight
* Description : Script of a traffic light, change it's colors and the stop gameobject
* Auteur : Aïssa Bovet
* Date : 04.06.2018
* Version : 1.0
*/
using System;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLight:MonoBehaviour
{
    //Gameobject for the red light of the traffic light
    private GameObject redLight;
    //Gameobject for the green light of the traffic light
    private GameObject greenLight;
    //Gameobject for stoping element of the traffic light
    private GameObject stop;
    /// <summary>
    /// At the creation of the gameObject initialise all variable with the children of the gameObject
    /// </summary>
    private void Start()
    {
        redLight = transform.GetChild(0).gameObject;
        greenLight = transform.GetChild(1).gameObject;
        stop = transform.GetChild(2).gameObject;
    }
    /// <summary>
    /// Change the color of the light and if the stoping element is active or not
    /// </summary>
    public void ChangeLights()
    {
        //The red light is put On or Off opposing his current state
        redLight.SetActive(!redLight.activeSelf);
        //Enable the stoping element if the redlight is ON,else disable it
        if (redLight.activeSelf)
        {
            stop.SetActive(true);
        }
        else
        {
            stop.SetActive(false);
        }
        //The green Light is put On or Off opposing his current state
        greenLight.SetActive(!greenLight.activeSelf);
    }
    /// <summary>
    /// Change the color to one specified by the bool color
    /// True: the Traffic light is on green
    /// False:the traffic light is on red
    /// </summary>
    /// <param name="Color"></param>
    public void SetColor(bool Color)
    {
        //Check wich one is to be activated
        if (Color)
        {
            ///active the green light
            greenLight.SetActive(true);
            ///deactive the red light
            redLight.SetActive(false);
            ///deactive the stoping element
            stop.SetActive(false);
        }
        else
        {
            ///active the green light
            greenLight.SetActive(false);
            ///deactive the red light
            redLight.SetActive(true);
            ///deactive the stoping element
            stop.SetActive(true);
        }
    }
    /// <summary>
    /// Get the state of the fire
    /// </summary>
    /// <returns></returns>
    public bool GetState() {
        if (greenLight.activeSelf)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}