﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : TrafficLightUI.cs
* Description : Gestion de l'interface utilisateur sur la scène de démo des feux
* Auteur : Aïssa Bovet
* Date : 04.06.2018
* Version : 1.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TrafficLightsUI : MonoBehaviour {
    [SerializeField]
    //button to start the cycle
    private Button start;
    //Label of the time left before a change
    [SerializeField]
    private Text timeLeft;
    //The traffic light manager used in the scene
    [SerializeField]
    private TrafficLightManager tlm;
    //Slider for changing the time
    [SerializeField]
    private Slider timeSlider;
    //The label of the slider
    [SerializeField]
    private Text timeSliderLbl;
    //button to change to the main camera
    [SerializeField]
    private Button cameraProto;
    //button to change to the second camera
    [SerializeField]
    private Button cameraRendu;
    //The camera axed on the prototype
    [SerializeField]
    private Camera MainCam;
    //The camera axed on the final rendering
    [SerializeField]
    private Camera SecondCam;
    /// <summary>
    /// On creation set the stop button and the main camera button as not interactable  
    /// Deactivate the second camera
    /// </summary>
    private void Start () {
        SecondCam.gameObject.SetActive(false);
        cameraProto.interactable = false;
    }
    /// <summary>
    /// Update the text lbl of how much time is left
    /// </summary>
    private void Update () {
        timeLeft.text = tlm.Timer.ToString();	
	}
    /// <summary>
    /// When the button Start is clicked
    /// Deactivate the button and activate the stop button
    /// </summary>
    public void Starting()
    {
        start.interactable = false;
    }
    /// <summary>
    /// When the button Stop is clicked
    /// Deactivate the button and activate the start button
    /// </summary>
    public void Stop()
    {
        start.interactable = true;
    }
    /// <summary>
    /// When the slider change of value change the time for the fire to change state and update the label
    /// </summary>
    public void SliderValue()
    {
        timeSliderLbl.text = (timeSlider.value+tlm.DEBUF).ToString();
        tlm.ChangeTime(timeSlider.value);
    }
    /// <summary>
    /// Switching the view to the prototype view
    /// Update the ui controls accordingly
    /// </summary>
    public void PrototypeVue()
    {
        MainCam.gameObject.SetActive(true);
        SecondCam.gameObject.SetActive(false);
        cameraProto.interactable = false;
        cameraRendu.interactable = true;
    }
    /// <summary>
    /// Switching the view to the final view
    /// Update the ui controls accordingly
    /// </summary>
    public void RenduVue()
    {
        MainCam.gameObject.SetActive(false);
        SecondCam.gameObject.SetActive(true);
        cameraProto.interactable = true;
        cameraRendu.interactable = false;
    }
    public void BackToMainMenu() {
        SceneManager.LoadScene(0);
    }
}
