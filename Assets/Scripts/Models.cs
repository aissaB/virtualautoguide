﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : Models
* Description :Manage the elements between scripts
* Auteur : Aïssa Bovet
* Date : 06.06.2018
* Version : 1.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Models : MonoBehaviour {
    //the car gameObject
    public GameObject car;
    //If the car should spawn
    public bool spawning=false;
    //The percentage of cars in the simulation
    public float percentageOfCars=50;
    //Change the statut of the spawning to true
    public void StartTheSpawn() {
        spawning = true;
    }
    //Change the statut of the spawning to false
    public void StopAllSpawner() {
        spawning = false;
    }
    //Get the car
    public GameObject GetCar()
    {
        return car;
    }
}
