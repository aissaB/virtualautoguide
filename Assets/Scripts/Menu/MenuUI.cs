﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : MenuUI.cs
* Description : Manage the User interface of the main menu
* Auteur : Aïssa Bovet
* Date : 13.06.2018
* Version : 1.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUI : MonoBehaviour {
    /// <summary>
    /// Load a scene
    /// </summary>
    /// <param name="scene"></param>
    public void LoadScene(int scene) {
        SceneManager.LoadScene(scene);
    }
    /// <summary>
    /// Quit the application
    /// </summary>
    public void QuitApp() {
        Application.Quit();
    }
}
