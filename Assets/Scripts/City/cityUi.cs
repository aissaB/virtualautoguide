﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : CityUI.cs
* Description : Manage the User interface of the City demo scene
* Auteur : Aïssa Bovet
* Date : 04.06.2018
* Version : 1.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class cityUi : MonoBehaviour {
    //The label, of the size of the city
    [SerializeField]
    private Text citySize;
    //The slider changing the size of the city
    [SerializeField]
    private Slider citySizeSlider;
    //The cityCreation script used in the scene
    [SerializeField]
    private CityCreation cityCreator;
    //The model of data
    [SerializeField]
    private Models model;
    //The slider changing the time between traffics lights states
    [SerializeField]
    private Slider timeSlider;
    //The slider changing the ratio of car spawning
    [SerializeField]
    private Slider carRatio;
    //The cube restraining the car from going before the user click play
    [SerializeField]
    private GameObject invisibleCube;
    //The city size
    private int size;
    //Default city size
    private const int defaulCitySize = 10;
	/// <summary>
    /// set the default value of the city's size to the default one
    /// </summary>
	void Start () {
        size = defaulCitySize;
	}
    /// <summary>
    /// Update the countdown label to the next traffic light's changing state
    /// </summary>
    private void Update()
    {
        timeSlider.gameObject.transform.GetChild(4).GetComponent<Text>().text = "Fires change in:" + cityCreator.TimeLeft();
    }
    /// <summary>
    /// When the slider change value, update the city's size and the label values
    /// </summary>
    public void ChangeSize()
    {
        //Change the label value, to the slider value
        citySize.text = citySizeSlider.value + "X" + citySizeSlider.value;
        //Change the city's size, to the slider value
        size = System.Convert.ToInt32(citySizeSlider.value);
    }
    /// <summary>
    /// Create a city based on the size 
    /// </summary>
    public void CreateTheCity()
    {
        //Destroy the old city
        cityCreator.DestroyCity();
        //Create the new city
        cityCreator.CreateCity(size);
    }
    /// <summary>
    /// Start the traffics lights cycle and 
    /// the spawning of cars
    /// </summary>
    public void StartTheSimulation()
    {
        //Start the traffics lights cycle
        cityCreator.StartSim();
        //Start the spawning of the cars
        model.StartTheSpawn();
        invisibleCube.SetActive(false);
    }
    /// <summary>
    /// Change the time between the traffics lights cycles based on the value of the time slider
    /// </summary>
    public void ChangeTimeTrafficsLights() {
        //Change the time of the cycle
        cityCreator.ChangeFireTime(timeSlider.value);
        //Update the label in the U.I
        timeSlider.gameObject.transform.GetChild(3).GetComponent<Text>().text = "Time to change:"+ timeSlider.value;
    }
    /// <summary>
    /// Change the percentage of cars spawning based on the value of the Car ratio slider
    /// </summary>
    public void ChangeCarPercentage() {
        //Update the label in the U.I
        carRatio.gameObject.transform.GetChild(3).GetComponent<Text>().text = "Car percentage:" + carRatio.value;
        //Change the ratio of cars
        model.percentageOfCars = carRatio.value;
    }
    /// <summary>
    /// Change the scene to the main menu one
    /// </summary>
    public void BackToMainMenu() {
        SceneManager.LoadScene(0);
    }
}
