﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : endsWorldSensor.cs
* Description : Detect if the gameObject is on the end of the current city
* Auteur : Aïssa Bovet
* Date : 06.06.2018
* Version : 1.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndsWorldSensor : MonoBehaviour {
    /// <summary>
    /// If the tile touch another one it destroy itself and his spawner with it
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}
