﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : Spawner.cs
* Description :Manage the spawn on the cars
* Auteur : Aïssa Bovet
* Date : 05.06.2018
* Version : 1.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    //The car prefab to instantiate
    private GameObject car;
    //The city in wich the car will Instantiate
    private GameObject city;
    //The model script contening the car percentage
    private Models model;
    //the percentage of car that should spawn
    private float percentage;
    //If the car should be spawning
    public bool onOff;
    //If the coroutine is already running
    private bool running;
    /// <summary>
    /// When the object become active initialise the variables
    /// </summary>
    private void Awake()
    {
        //The coroutine is not  running
        running = false;
        //Set the spawn to the default state 
        onOff = false;
        //Initialise the model 
        model = GameObject.Find("Models").GetComponent<Models>();
        //Get the car gameobject from the mode
        car = model.GetCar();
        //Get the city
        city = GameObject.Find("City");
        //Launch the couroutine
        StartCoroutine(CreateCars());
    }
    /// <summary>
    /// Stop the couroutine before destroying the gamObject
    /// </summary>
    private void OnDestroy()
    {
        StopCoroutine(CreateCars());
    }
    /// <summary>
    /// Check if the car should be running and if a coroutine is not already running
    /// </summary>
    private void Update()
    {
        onOff = model.spawning;
        if (onOff && !running)
        {
            StartCoroutine(CreateCars());
        }
        else
        {
            StopCoroutine(CreateCars());
        }
        
    }
    /// <summary>
    /// Instatiate cars by the percentage set
    /// </summary>
    /// <returns></returns>
    IEnumerator CreateCars() {
        while (onOff)
        {
            //Get the city
            city = GameObject.Find("City");
            //Set that the coroutine is runnning
            running = true;
            //Get the car percentage
            percentage = model.percentageOfCars;
            //Get a random number
            int doiSpawn=Random.Range(0, 100);
            //If the number is less or equal to the percentage
            if (doiSpawn<= percentage)
            {
                //instantiate the car
                GameObject newCar = Instantiate(car, gameObject.transform.position, transform.rotation, city.transform);
            }
            //Wait 5 second before the next iteration
            yield return new WaitForSeconds(5f);
        }
        //The coroutine end
        running = false;
    }
}
