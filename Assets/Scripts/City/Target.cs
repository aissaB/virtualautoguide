﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : Spawner.cs
* Description :Change the position of the target of the main car
* Auteur : Aïssa Bovet
* Date : 13.06.2018
* Version : 1.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {
	/// <summary>
    /// Change the position of the target
    /// </summary>
	private void Update () {
        //When the A key is pressed
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-1, 0, 0);
        }
        //When the S key is pressed
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, 0, -1);
        }
        //When the D key is pressed
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(1, 0, 0);
        }
        //When the W key is pressed
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(0, 0, 1);
        }

    }
}
