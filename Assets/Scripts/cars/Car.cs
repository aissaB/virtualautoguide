﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : Car.cs
* Description : Car's behavior
* Auteur : Aïssa Bovet
* Date : 06.06.2018
* Version : 3.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    //Car's rigidbody
    private Rigidbody carRb;
    //Right sensor
    private Sensor sensorR;
    //Left sensor
    private Sensor sensorL;
    //Down sensor
    private Sensor sensorD;
    //Vitesse parameter
    private float speed;
    //Editor label
    [Header("Car parameters")]
    //Stopping distance
    [SerializeField]
    private float stopDistance;
    //Get Set of speed
    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    /// <summary>
    /// Initialise the car's variables
    /// </summary>
    private void Start()
    {
        //Donne une vitesse aléatoire entre 10 m/s et 30 m/s
        Speed = Random.Range(1, 3);
        //Get the car rigidBody
        carRb = gameObject.GetComponent<Rigidbody>();
        //Get the sensor
        //right
        sensorR = gameObject.transform.GetChild(1).GetComponent<Sensor>();
        //Left
        sensorL = gameObject.transform.GetChild(2).GetComponent<Sensor>();
        //Down
        sensorD = gameObject.transform.GetChild(3).GetComponent<Sensor>();
        //Change the editor value to be in decimeter 
        stopDistance = stopDistance / 10;
    }
    /// <summary>
    /// Manage the car speed (or stop)
    /// </summary>
    private void FixedUpdate()
    {
        //If one of the sensor detecct an obstacle
        if (sensorL.ObstacleDetected || sensorR.ObstacleDetected)
        {
            //Get the left sensor distance, if the sensor don't detect an obstacle set the distance
            //over the max distance
            float dist = (sensorL.ObstacleDetected)?sensorL.Distance:3;
            
            //Chack if the distance of the right one is shorter
            if (sensorR.ObstacleDetected && dist>sensorR.Distance)
            {
                //If distance of the right sensor is shorter the right redefine the distance
                dist = sensorR.Distance;
            }
            //make a percentage of the speed 
            float speedRatio = dist / Speed;
            //if the ratio is lower than the stoping distance, the car stop
            speedRatio = (speedRatio < stopDistance) ? 0 : speedRatio;
            //add the calculated speed to the car
            carRb.velocity = transform.forward * (speedRatio * Speed);
        }
        else
        {
            //When nothing is detected the speed is set to the maximum
            carRb.velocity = transform.forward * Speed;
        }
    }
}
