# Virtual auto guide
![red car](img/logo.png)
## Introduction
This was my final work  for the CFC of I.T specialist.
It's a simple simulation made in unity that  create a traffic within a city and a main car that must go to a point given by the user. The cars must not touch each other and the main car (red) must avoid the reds cars that compose the traffic.

![red car](img/traficCar.PNG)  ![red car](img/AutoCar.PNG)

![red car](img/Trafic.PNG)

## How to run
Make it run in by opening it with unity (I've not build it since it is more of a tech demo than a real product to deliver)

Go to the scene Menu (Assets/Scenes/Menu) and press play

## Documentation
The documentation is in the project the file "Manuel utilisateur" and "Documentation technique" in the docs folders
They are in french but I will try to make a translation.